package com.test.client;

import com.test.dto.BreedDto;
import com.test.dto.BreedImagesDto;

public interface RestClient {

	BreedDto breedsList();

	BreedImagesDto listByBreed(String breed);

}
