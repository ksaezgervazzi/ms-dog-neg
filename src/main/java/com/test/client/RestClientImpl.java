package com.test.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.test.dto.BreedDto;
import com.test.dto.BreedImagesDto;

@Component
public class RestClientImpl implements RestClient {

	@Autowired
	RestTemplate restTemplate;

	@Value("${apiDog.url.listAllBreeds}")
	private String apiDogUrlListAllBreeds;

	@Value("${apiDog.url.listByBreed}")
	private String apiDogUrlListByBreed;
	
	private static String IMAGES_URL = "/images";

	@Override
	public BreedDto breedsList() {
		BreedDto breedDto = BreedDto.builder().build();
		try {
			ResponseEntity<BreedDto> response = restTemplate.exchange(apiDogUrlListAllBreeds, HttpMethod.GET, null,
					BreedDto.class);

			if(response != null) {
				breedDto = response.getBody();
			}
		} catch (Exception ex) {
			throw ex;
		}
		return breedDto;
	}

	@Override
	public BreedImagesDto listByBreed(String breed) {

		BreedImagesDto breedImagesDto = BreedImagesDto.builder().build();

		try {
			ResponseEntity<BreedImagesDto> response = restTemplate.exchange(
					apiDogUrlListByBreed.concat(breed).concat(IMAGES_URL), HttpMethod.GET, null, BreedImagesDto.class);

			if(response != null) {
			breedImagesDto = response.getBody();
			}
			
		} catch (Exception ex) {
			throw ex;
		}
		return breedImagesDto;
	}
}
