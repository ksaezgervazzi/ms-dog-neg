package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.test.dto.BreedDescriptionDto;
import com.test.service.BreedService;



@RestController
public class Controller {
	
	@Autowired
	BreedService breedService;
	
	/**
	 * @param
	 * @return BreedDescriptionDto
	 * @author Karol S�ez.
	 */
	
	@GetMapping(
			value = "/breed/{breed}",
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	public BreedDescriptionDto breeds(@PathVariable("breed") String breed) {
		return breedService.breedDescription(breed);
	}



}
