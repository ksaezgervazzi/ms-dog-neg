package com.test.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.client.RestClient;
import com.test.dto.BreedDescriptionDto;
import com.test.dto.BreedDto;
import com.test.dto.BreedImagesDto;
import com.test.dto.ImagesDto;

@Service
public class BreedServiceImpl implements BreedService {

	@Autowired
	RestClient restClient;

	@Override
	public BreedDescriptionDto breedDescription(String breed) {

		List<String> subBreedList = getSubBreed(breed) == null ? new ArrayList<>() : getSubBreed(breed);

		List<ImagesDto> imagesList = getUrl(breed) == null ? new ArrayList<>() : getUrl(breed);

		return BreedDescriptionDto.builder().breed(breed).subBreed(subBreedList).images(imagesList).build();
	}

	/**
	 * @param breed
	 * @return List<ImagesDto>
	 * @author Karol S�ez.
	 */
	private List<ImagesDto> getUrl(String breed) {

		List<ImagesDto> imagesList = new ArrayList<>();

		BreedImagesDto breedImagesDto = restClient.listByBreed(breed);

		if (breedImagesDto != null) {
			List<String> images = breedImagesDto.getMessage();

			if (images != null) {
				images.forEach(img -> {
					imagesList.add(ImagesDto.builder().url(img).build());
				});
			}
		}

		return imagesList;
	}

	/**
	 * @param breed
	 * @return List<String>
	 * @author Karol S�ez.
	 */
	private List<String> getSubBreed(String breed) {

		List<String> subBreedList = new ArrayList<>();

		BreedDto breedDto = restClient.breedsList();

		if (breedDto != null) {
			Map<String, List<String>> subBreedMap = breedDto.getMessage();
			if (subBreedMap != null) {
				subBreedList = subBreedMap.get(breed);
			}
		}

		return subBreedList;
	}
}
