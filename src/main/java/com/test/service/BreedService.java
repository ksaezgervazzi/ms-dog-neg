package com.test.service;

import com.test.dto.BreedDescriptionDto;

public interface BreedService {
	
	BreedDescriptionDto breedDescription(String breed);

}
