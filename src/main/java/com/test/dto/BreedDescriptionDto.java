package com.test.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class BreedDescriptionDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String breed;
	
	private List<String> subBreed;
	
	private List<ImagesDto> images;

}
