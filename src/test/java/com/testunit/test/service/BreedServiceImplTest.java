package com.testunit.test.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.test.client.RestClient;
import com.test.dto.BreedDto;
import com.test.dto.BreedImagesDto;
import com.test.service.BreedServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class BreedServiceImplTest {
	
	@InjectMocks
	BreedServiceImpl breedServiceImpl;
	
	@Mock
	RestClient restClient;
	
	@Test
	public void breedDescriptionTestNotNull() {
		
		String breed =  "breedOk";
		
		List<String> subBreedList = new ArrayList<>();
		subBreedList.add("subBreedOk");
		
		Map<String, List<String>> messageListAll = new HashMap<>();
		messageListAll.put(breed, subBreedList);
		BreedDto breedDto = BreedDto.builder().message(messageListAll).build();
		
		List<String> messageListByBreed = new ArrayList<>();
		messageListByBreed.add("http:imagesBreedOk");
		BreedImagesDto breedImagesDto = BreedImagesDto.builder().message(messageListByBreed).build();
		
		Mockito.doReturn(breedDto).when(restClient).breedsList();
		Mockito.doReturn(breedImagesDto).when(restClient).listByBreed(breed);
		
		Assert.assertNotNull(breedServiceImpl.breedDescription(breed));
		
	}
	
	@Test
	public void breedDescriptionTestSubBreedEmpty() {
	
		String breed =  "breedNoOk";
		
		Mockito.doReturn(null).when(restClient).breedsList();
		Mockito.doReturn(null).when(restClient).listByBreed(breed);
			
		Assert.assertTrue(breedServiceImpl.breedDescription(breed).getSubBreed().isEmpty());
		
	}

}
